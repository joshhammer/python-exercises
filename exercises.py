
#exercise01
#name = input('Please type your name: ')

def sayHello(name, *arguments):
    print('Hello, ' + name + '!')
    for word in arguments:
        print('Hello, ' + word + '!')

#sayHello(name, 'Jenny', 'Alf')


#exercise02

import os

def files_and_dirlist():
    path = os.getcwd()
    return os.listdir(path)

print(files_and_dirlist())

#exercise03

list1 = [2, 5, 7, 9, 11]
list2 = [2, 5, 7, 8, 11]
list3 = [2, 5, 11, 9, 7]

def areTwoArraysEqual(arg1, arg2):
    arg1.sort()
    arg2.sort()
    return arg1 == arg2

#print(areTwoArraysEqual(list1, list2))
#print(areTwoArraysEqual(list1, list3))


#exercise04

import math

def getMiddleCharacter(string):
    stringLength = len(string)
    if stringLength % 2 != 0:
        middleIndex = int(math.floor(stringLength/2))
        print(string[middleIndex])
    else:
        middleIndex = stringLength/2
        print(string[middleIndex-1]+string[middleIndex])


#getMiddleCharacter('racecar')


#exercise05

def countAllVowels(string):
    vowelCount = 0
    splitString = list(string.lower())
    for item in splitString:
        if item == 'a' or item == 'e' or item == 'i' or item == 'o' or item == 'u':
            vowelCount += 1

    return vowelCount

#print(countAllVowels('Ordinary'))


#exercise06

def checkValidPassword(string):
    if len(string) >= 10:
        return 'Password valid!'
    else:
        return '10 characters minimum!!'

print(checkValidPassword('Test123456'))


#exercise07

def find_second_smallest(list):
    list.sort()
    return list[1]


#print(find_second_smallest([0, 3, -2, 4, 3, 2, -1, 15, -5, 4, -7]))
#print(find_second_smallest([10, 22, 12, 20, 11, 2, 8, 22]))


#exercise08

def removeDuplicates(array):
    newDict = dict.fromkeys(array)
    cleanList = list(newDict)
    return cleanList

#print(removeDuplicates([0, 3, -2, 4, 3, 2]))
#print(removeDuplicates([10, 22, 10, 20, 11, 22]))


#exercise09

def get_pentagonal_number(n):
    x = 0
    while x <= n:
        print((x*(3*x-1))/2)
        x += 1


#get_pentagonal_number(50)


#exercise10

def makeBinary(num):
    binaryArray = []

    while num > 0:
        binaryChar = num % 2
        binaryArray.append(binaryChar)
        num /= 2

    binaryArray.reverse()
    return binaryArray


print(makeBinary(457))

#exercise11

def fibonacci(num):
    fibonacciArray = [0, 1]
    for i in range(1, 50):
        fibonacciArray.append(fibonacciArray[i] + fibonacciArray[i-1])

    return fibonacciArray[num]


#print(fibonacci(7))  # 13


#exercise12

def factorial(num):
    x = 1
    result = 1
    while x <= num:
        result *= x
        x += 1
    return str(num) + '! = ' + str(result)

#print(factorial(5))


#exercise13

def prime_number(end):
    start = 2
    end = end

    for val in range(start, end + 1):
        if val > 1:
            for n in range(2, val):
                if (val % n) == 0:
                    break
            else:
                print(val)


#print(prime_number(30))


#exercise14

def sort_it(array):
    roundedArray = []
    evenArray = []
    oddArray = []
    for val in array:
        roundedArray.append(math.floor(val))

    for val in roundedArray:
        if val % 2 == 0:
            evenArray.append(val)
        else:
            oddArray.append(val)

    evenArray.sort()
    evenArray.reverse()

    return oddArray + evenArray

#print(sort_it([1, 2, 3, 4, 5, 6, 7, 8, 9]))
#print(sort_it([26.66, 24.01, 52.00, 2.10, 44.15, 1.02, 11.15]))


#exercise15

import functools

def multiply(x1, x2):
    return x1 * x2

def find(number):
    numberArray = []

    numberString = str(number)
    array = [char for char in numberString]

    for element in array:
        numberArray.append(int(element))

    result = reduce(multiply, numberArray)
    return result


print(find(456))
